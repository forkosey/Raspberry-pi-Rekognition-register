from setuptools import setup

setup(name='rekognition',
      version='0.4.5.4',
      description='AWS face recognition api.',
      author='Marc-Antoine Giguère',
      author_email='Marc-Antoine.Giguere@lacapitale.com',
      packages=['Rekognition'],
      install_requires=[
          'boto3',
      ],
      zip_safe=False)
