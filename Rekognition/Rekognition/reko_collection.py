"""
This module contains all the functions to search a face from an image in a
Rekognition face collection.

This module uses aws Rekognition's SDK boto3 to index, delete, search and get
description from faces of an image in a Rekognition face collection.

"""
import botocore
from Rekognition.credentials import (S3, S3_CLIENT, REKO_CLIENT,
                                     COLLECTION_ID, PROFILE_BUCKET_NAME,
                                     NEW_IMG_BUCKET_NAME, NEW_IMG_FILE_NAME,
                                     TEST_NEW_IMG_BUCKET_NAME, TEST_PROFILE_BUCKET_NAME,
                                     TEST_COLLECTION_ID)

import Rekognition.exceptions as RekoExceptions

def add_face_to_collection(new_profile_img, image_name, unit_test=False):
    """
    Adds a face from an image to the Rekognition face collection.

    Adds all the faces found in a given image to the Rekognition face
    collection, deletes them all except the largest one and return it's id.

    Args:
        new_profile_img (ByteImg): The image from which the face(s) will
        be detected.

        image_name (String): The name under which it'll be saved in the bucket.

    Returns:
        Dict: Contains the face_id and the aws dict description.
        Format:
        {
         'face_id'  : String,
         'aws_desc_dict' : Dict
        }

    """
    if unit_test:
        col_id = TEST_COLLECTION_ID
        profile_bucket = TEST_PROFILE_BUCKET_NAME
    else:
        col_id = COLLECTION_ID
        profile_bucket = PROFILE_BUCKET_NAME


    if not col_id in REKO_CLIENT.list_collections(MaxResults=10)['CollectionIds']:
        REKO_CLIENT.create_collection(CollectionId=col_id)

    S3_CLIENT.put_object(Bucket=profile_bucket, Key=image_name, Body=new_profile_img)

    response = REKO_CLIENT.index_faces(
        CollectionId=col_id,
        Image={
            'S3Object': {
                'Bucket': profile_bucket,
                'Name': image_name
            }
        }
    )['FaceRecords']

    face_ids = [x['Face']['FaceId'] for x in response]

    if len(face_ids) > 1:
        delete_faces_from_collection(face_ids[1:], unit_test)

    return {'face_id'       : face_ids[0],
            'aws_desc_dict' : get_aws_desc({'Bucket': profile_bucket,
                                            'Name'  : image_name})}


def delete_faces_from_collection(face_ids, unit_test=False):
    """
    Deletes all the faces specified by their ids.

    Deletes all the faces from the Rekognition face collection
    specified by their ids.

    Args:
        face_ids (List): The list of face ids to delete.

    Returns:
        Bool: Says whether or not the operation was a success.

    """
    try:

        if unit_test:
            col_id = TEST_COLLECTION_ID
        else:
            col_id = COLLECTION_ID

        REKO_CLIENT.delete_faces(
            CollectionId=col_id,
            FaceIds=face_ids
        )
        return True
    except Exception:
        return False


def search_faces_from_img(search_from_img, unit_test=False):
    """
    Search for a match between faces in an image and a Rekognition face
    collection and return the face's id and aws description dict if found.

    It deletes all images from the bucket before adding the new one to it. It
    adds all the faces in the new image to the face collection and tries to
    match the biggest face from the image with one from the collection. It then
    deletes all the new faces from the face collection and return the face's id
    and aws description dict if found.

    Args:
        search_from_img (ImageBytes): The image to upload to S3 and search faces
        from.

    Returns:
        Dict: Contains the face_id and the aws dict description.
        Format:
        {
         'face_id'  : String,
         'aws_desc' : Dict
        }

    Raises:
        CollectionDoesNotExistsException: Raises if the collection is not
        initialised.

        NoFaceDetectedException: Raises if no faces are detected in the new
        image.

        NoMatchingFaceException: Raises if no face matches the biggest one from
        the new image.

    """
    if unit_test:
        col_id = TEST_COLLECTION_ID
        new_img_bucket = TEST_NEW_IMG_BUCKET_NAME
    else:
        col_id = COLLECTION_ID
        new_img_bucket = NEW_IMG_BUCKET_NAME

    bucket = S3.Bucket(new_img_bucket)

    for key in bucket.objects.all():
        key.delete()

    S3_CLIENT.put_object(Bucket=new_img_bucket, Key=NEW_IMG_FILE_NAME, Body=search_from_img)

    if not col_id in REKO_CLIENT.list_collections(MaxResults=10)['CollectionIds']:
        raise RekoExceptions.CollectionDoesNotExistsException()

    index_response = REKO_CLIENT.index_faces(
        CollectionId=col_id,
        Image={
            'S3Object': {
                'Bucket': new_img_bucket,
                'Name': NEW_IMG_FILE_NAME
            }
        }
    )['FaceRecords']

    if not index_response:
        raise RekoExceptions.NoFaceDetectedException()
    else:
        face_ids = [x['Face']['FaceId'] for x in index_response]

        search_response = REKO_CLIENT.search_faces(
            CollectionId=col_id,
            FaceId=face_ids[0],
            MaxFaces=1,
        )

        delete_faces_from_collection(face_ids, unit_test)

        if search_response['FaceMatches']:
            return {
                'face_id'       : search_response['FaceMatches'][0]['Face']['FaceId'],
                'aws_desc_dict' : get_aws_desc({'Bucket':new_img_bucket, 'Name':NEW_IMG_FILE_NAME})
                }
        else:
            raise RekoExceptions.NoMatchingFaceException()


def list_faces_of_collection():
    """
    Get the list of all face ids in the Rekognition face collection.

    Returns:
        List: Contains all the face ids.

    """
    return REKO_CLIENT.list_faces(
        CollectionId=COLLECTION_ID
    )


def delete_all_faces():
    """
    Deletes all faces from the Rekognition face collection.

    Returns:
        None: Returns nothing.

    """
    face_list = list_faces_of_collection()
    face_ids = [x['FaceId'] for x in face_list['Faces']]
    delete_faces_from_collection(face_ids)


def flush_all_data():
    """
    Deletes all faces in the recognition face collection and all the images
    in the buckets.

    Returns:
        None: Returns nothing.

    """
    delete_all_faces()
    buckets = []
    buckets.append(S3.Bucket(PROFILE_BUCKET_NAME))
    buckets.append(S3.Bucket(NEW_IMG_BUCKET_NAME))
    for bucket in buckets:
        for key in bucket.objects.all():
            key.delete()


def download_profile_img(key, path):
    """
    Download a specified image to a specified location from the profile s3
    bucket.

    Args:
        key (String): The image name in the bucket.

        path (String): The path were to download the image.

    Returns:
        None: Returns nothing.

    """
    try:
        S3.Bucket(PROFILE_BUCKET_NAME).download_file(key, path)
    except botocore.exceptions.ClientError as exception:
        if exception.response['Error']['Code'] == "404":
            print("The object does not exist.")
        else:
            raise


def get_aws_desc(s3_obj):
    """
    Gets the description from aws on a specified image.

    Gets a Dict of the aws description of a specified face in an image.

    Args:
        s3_obj (Dict): The s3 object of the specified image.

    Returns:
        Dict: Contains the description of the image with all the infos
        from Rekognition.

    """

    desc_response = REKO_CLIENT.detect_faces(
        Image={
            'S3Object':s3_obj
        },
        Attributes=['ALL']
    )['FaceDetails'][0]

    return {
        'AgeRange' : desc_response['AgeRange'],
        'Smile' : desc_response['Smile'],
        'Eyeglasses' : desc_response['Eyeglasses'],
        'Sunglasses' : desc_response['Sunglasses'],
        'Gender' : desc_response['Gender'],
        'Beard' : desc_response['Beard'],
        'Mustache' : desc_response['Mustache'],
        'EyesOpen' : desc_response['EyesOpen'],
        'MouthOpen' : desc_response['MouthOpen'],
        'Emotions' : desc_response['Emotions']
    }


#flush_all_data()
#print(list_faces_of_collection())
