"""
This package uses aws Rekognition's SDK boto3 to simplify the use of facial
recognition and only implement the needed part of the API.

"""
__all__ = [
    "credentials",
    "exceptions",
    "reko_collection",
    "unit_test"
]
