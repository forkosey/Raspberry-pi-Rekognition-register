import unittest
import io
from PIL import Image
from Rekognition.reko_collection import (add_face_to_collection, delete_faces_from_collection,
                                         search_faces_from_img, delete_all_faces)

import Rekognition.exceptions as RekoExceptions
from Rekognition.credentials import S3, TEST_PROFILE_BUCKET_NAME, TEST_NEW_IMG_BUCKET_NAME

def get_img(file_name):
    byte_img_io = io.BytesIO()
    byte_img = Image.open('test_img/' + file_name + '.png')
    byte_img.save(byte_img_io, "PNG")
    byte_img_io.seek(0)
    return byte_img_io.read()

def empty_buckets():
    buckets = []
    buckets.append(S3.Bucket(TEST_PROFILE_BUCKET_NAME))
    buckets.append(S3.Bucket(TEST_NEW_IMG_BUCKET_NAME))
    for bucket in buckets:
        for key in bucket.objects.all():
            key.delete()


class TestRekoCollection(unittest.TestCase):

    def setUp(self):
        delete_all_faces()

    def test_should_ReturnString_when_FaceIsAddedWithoutProblem(self):
        img = get_img('profile_test')
        empty_buckets()
        self.assertGreater(len(add_face_to_collection(img, 'add_test.png')), 0)


    def test_should_ReturnTrue_when_FaceAreDeletedWithoutProblem(self):
        img = get_img('profile_test')
        empty_buckets()
        new_face_id = add_face_to_collection(img, 'delete_test.png')
        self.assertTrue(delete_faces_from_collection([new_face_id]))
        self.assertFalse(delete_faces_from_collection('test_string'))

    def test_should_ReturnFalse_when_ThereIsAProblemInTheDeleteProcess(self):
        empty_buckets()
        self.assertFalse(delete_faces_from_collection('test_string'))


    def test_should_ReturnRightFaceId_when_FindMatchingFace(self):
        add_img_01 = get_img('profile_test')
        add_img_02 = get_img('profile_test_02')
        search_img = get_img('search_test')
        empty_buckets()
        matching_face_id = add_face_to_collection(add_img_01, 'search_test.png')
        not_matching_face_id = add_face_to_collection(add_img_02, 'search_test_02.png')
        search_face_id_result = search_faces_from_img(search_img)
        self.assertEqual(search_face_id_result, matching_face_id)
        self.assertNotEqual(search_face_id_result, not_matching_face_id)


    def test_should_ThrowException_when_NoFaceMatches(self):
        add_img = get_img('profile_test')
        not_matching_img = get_img('not_matching')
        empty_buckets()
        add_face_to_collection(add_img, 'search_test.png')
        with self.assertRaises(RekoExceptions.NoMatchingFaceException):
            search_faces_from_img(not_matching_img)



if __name__ == '__main__':
    unittest.main()
