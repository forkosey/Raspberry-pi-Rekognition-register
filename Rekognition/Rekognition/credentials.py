"""
This module contains all the credentials needed to use the Rekognition API,
constants used by the Rekognition package and some ressource and client.

"""

import boto3
AWS_LOGIN_CREDENTIAL = 'alexandre.folgueras@lacapitale.com/LaCapitale2017'
AWS_ACCESS_KEY_ID = 'AKIAJXZYCCQDLKDSHPUA'
AWS_SECRET_ACCESS_KEY = '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'
REGION = 'us-east-1'

COLLECTION_ID = 'ras-pi-reko-profile-collection'
TEST_COLLECTION_ID = 'ras-pi-reko-profile-collection-unit-test'

BUCKET_NAME = 'ras_pi_rekognition_upload_bucket'
PROFILE_BUCKET_NAME = 'ras-pi-reko-profile-bucket'
NEW_IMG_BUCKET_NAME = 'ras-pi-reko-new-img-upload-bucket'
TEST_PROFILE_BUCKET_NAME = 'ras-pi-reko-profile-bucket-unit-test'
TEST_NEW_IMG_BUCKET_NAME = 'ras-pi-reko-new-img-upload-bucket-unit-test'

NEW_IMG_FILE_NAME = 'new_img_upload.png'
SOURCE_IMG = 'source_img_'
TARGET_IMG = 'target_img_'

S3 = boto3.resource('s3')
S3_CLIENT = boto3.client('s3')
REKO_CLIENT = boto3.client('rekognition')

DEBUG = True
