"""
This module contains all the custom Exception types used by the Rekognition
package.

"""


class CollectionDoesNotExistsException(Exception):
    """
    Raised when no rekognition face collection with such id exists.

    """
    def __init__(self):
        super(CollectionDoesNotExistsException, self).__init__(
            "The collection id specified does not exists.")


class NoMatchingFaceException(Exception):
    """
    Raised when no face is matching the searched one in the rekognition face
    collection.

    """
    def __init__(self):
        super(NoMatchingFaceException, self).__init__(
            "No face matches the face from the source image.")


class NoFaceDetectedException(Exception):
    """
    Raised when no face is detected in an image.

    """
    def __init__(self):
        super(NoFaceDetectedException, self).__init__(
            "No face detected in the source image.")
