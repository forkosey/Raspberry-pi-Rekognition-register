from rest_framework import serializers
from django.contrib.auth.models import User
from ras_pi_ui.models import Product, Profile, Sale, AwsDescription, AwsDescriptionTopThree
from api.models import ApiUserReturnType

class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('url', 'username', 'email', 'first_name', 'last_name')


class ProfileSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Profile
        fields = ('user', 'balance', 'reko_face_id', 'reko_desc')


class ProductSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Product
        fields = ('name', 'price', 'in_stock')


class SaleSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Sale
        fields = ('product', 'user', 'date')


class AwsDescriptionSerializer(serializers.ModelSerializer):
    class Meta:
        model = AwsDescription
        fields = ('top_three', 'attribute_name', 'attribute_value')


class AwsDescriptionTopThreeSerializer(serializers.ModelSerializer):
    class Meta:
        model = AwsDescriptionTopThree
        fields = ('profile', 'date')


class ApiUserReturnTypeSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = ApiUserReturnType
        fields = ('user_name', 'product_name', 'number_of_purchase', 'total', 'description', 'old')
