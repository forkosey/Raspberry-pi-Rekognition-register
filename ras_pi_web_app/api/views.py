#!/usr/bin/python
# -*- coding: utf-8 -*-

from datetime import date, timedelta
from rest_framework import viewsets
from rest_framework.decorators import list_route
from rest_framework.response import Response
from django.contrib.auth.models import User
from api.models import ApiUserReturnType
from api.Serializers import (UserSerializer, ProfileSerializer,
                             ProductSerializer, SaleSerializer,
                             ApiUserReturnTypeSerializer, AwsDescriptionSerializer,
                             AwsDescriptionTopThreeSerializer)
from ras_pi_ui.models import Product, Profile, Sale, AwsDescription, AwsDescriptionTopThree


class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer


class ProfileViewSet(viewsets.ModelViewSet):
    queryset = Profile.objects.all()
    serializer_class = ProfileSerializer


class ProductViewSet(viewsets.ModelViewSet):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer


class SaleViewSet(viewsets.ModelViewSet):
    queryset = Sale.objects.all()
    serializer_class = SaleSerializer


class AwsDescriptionViewSet(viewsets.ModelViewSet):
    queryset = AwsDescription.objects.all()
    serializer_class = AwsDescriptionSerializer

    @list_route()
    def get_latest_list(self, request):
        last_top_three = AwsDescriptionTopThree.objects.latest('date')
        aws_desc_list = AwsDescription.objects.filter(top_three__pk=last_top_three.id)

        serializer = self.get_serializer(aws_desc_list, many=True)
        return Response(serializer.data)


class AwsDescriptionTopThreeViewSet(viewsets.ModelViewSet):
    queryset = AwsDescriptionTopThree.objects.all()
    serializer_class = AwsDescriptionTopThreeSerializer


class ApiUserReturnTypeViewSet(viewsets.ReadOnlyModelViewSet):

    queryset = ApiUserReturnType.objects.all()
    serializer_class = ApiUserReturnTypeSerializer

    @list_route()
    def create_and_get_latest(self, request):
        sales = Sale.objects.all()
        prod_bought = {}
        for sale in sales:
            if sale.product.name in prod_bought:
                prod_bought[sale.product.name] += 1
            else:
                prod_bought[sale.product.name] = 1

        most_bought_prod = max(prod_bought, key=prod_bought.get)
        most_bought_prod = Product.objects.get(name=most_bought_prod)

        last_week_date = date.today() - timedelta(days=7)
        sales = Sale.objects.filter(product__pk=most_bought_prod.id, date__gte=last_week_date)
        user_bought = {}
        for sale in sales:
            if sale.user.id in user_bought:
                user_bought[sale.user.id] += 1
            else:
                user_bought[sale.user.id] = 1

        most_bought_user = max(user_bought, key=user_bought.get)
        most_bought_user = User.objects.get(pk=most_bought_user)
        bought_time = user_bought[most_bought_user.id]
        total = user_bought[most_bought_user.id]*most_bought_prod.price
        desc = ("Le produit le plus populaire de la semaine est: " + most_bought_prod.name +
                "\n" + most_bought_user.first_name + " " + most_bought_user.last_name +
                " est premier(e) avec " + str(bought_time) + " " + most_bought_prod.name +
                " pour un total de " + str(total) + "$.")

        return_value = ApiUserReturnType(user_name=most_bought_user.username,
                                         product_name=most_bought_prod.name,
                                         number_of_purchase=bought_time,
                                         total=total,
                                         description=desc,
                                         old=False)
        query = ApiUserReturnType.objects.all()
        query.update(old=True)
        return_value.save()
        serializer = self.get_serializer(return_value, many=False)
        return Response(serializer.data)


    @list_route()
    def get_latest(self, request):
        return_value = ApiUserReturnType.objects.filter(old=False)[0]
        serializer = self.get_serializer(return_value, many=False)
        return Response(serializer.data)
