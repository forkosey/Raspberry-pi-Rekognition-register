from django.db import models
from django.contrib.auth.models import User
from ras_pi_ui.models import Profile, Product, Sale
from django.utils import timezone

class ApiUserReturnType(models.Model):
    user_name = models.CharField(max_length=50)
    product_name = models.CharField(max_length=50)
    number_of_purchase = models.SmallIntegerField(default=0)
    total = models.DecimalField(max_digits=5, decimal_places=2, default=0)
    description = models.CharField(max_length=200)
    date_created = models.DateTimeField(default=timezone.now)
    old = models.BooleanField(default=False)

    def __str__(self):
        return str(self.user_name) + " --> " + str(self.date_created)
