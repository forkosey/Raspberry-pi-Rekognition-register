from django.contrib import admin

from .models import ApiUserReturnType

admin.site.register(ApiUserReturnType)
