from django.conf.urls import include, url, static
from django.contrib import admin
from django.conf import settings
from rest_framework import routers
from api.views import (UserViewSet, ProfileViewSet,
                       ProductViewSet, SaleViewSet,
                       AwsDescriptionViewSet, AwsDescriptionTopThreeViewSet,
                       ApiUserReturnTypeViewSet)

ROUTER = routers.DefaultRouter()
ROUTER.register(r'users', UserViewSet)
ROUTER.register(r'profiles', ProfileViewSet)
ROUTER.register(r'products', ProductViewSet)
ROUTER.register(r'sales', SaleViewSet)
ROUTER.register(r'AwsDescription', AwsDescriptionViewSet)
ROUTER.register(r'AwsDescriptionTopThree', AwsDescriptionTopThreeViewSet)
ROUTER.register(r'ApiUserReturnTypes', ApiUserReturnTypeViewSet)

urlpatterns = [
    url(r'^ras_pi_ui/', include('ras_pi_ui.urls', namespace='ras_pi_ui')),
    url(r'^ras_pi_admin/', include('ras_pi_admin.urls', namespace='ras_pi_admin')),
    url(r'^admin/', admin.site.urls),
    url(r'^api/', include(ROUTER.urls)),
] + static.static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
