from django.shortcuts import render, Http404, redirect
from ras_pi_ui.models import Profile, Product, Sale

def index(request):
    need_to_pay = get_need_to_pay()
    need_to_restock = get_need_to_restock()
    profiles = Profile.objects.filter(balance__lt=10)[:max((5 - len(need_to_pay)), 0)]
    products = Product.objects.filter(in_stock__gt=5)[:max((5 - len(need_to_restock)), 0)]
    sales = Sale.objects.all()[:5]


    return render(request, 'ras_pi_admin/index.html', {'profiles':profiles,
                                                       'products':products,
                                                       'sales':sales,
                                                       'need_to_restock':need_to_restock,
                                                       'need_to_pay':need_to_pay
                                                       })


def profile_summary(request, profile_id):
    p = Profile.objects.get(pk=profile_id)
    sales_q = Sale.objects.filter(user=p.user)
    sales = []
    for item in sales_q:
        sales.append(item)

    return render(request, 'ras_pi_admin/profile_summary.html', {'profile':p, 'sales':sales})


def product_summary(request, product_id):
    p = Product.objects.get(pk=product_id)
    sales_q = Sale.objects.filter(product=p)
    sales = []
    for item in sales_q:
        sales.append(item)

    return render(request, 'ras_pi_admin/product_summary.html', {'product': p, 'sales': sales})


def pay_balance(request):
    if request.POST:
        param = request.POST
        p = Profile.objects.filter(id=param['profile_id'])[0]
        paid = param['paid']
        p.balance = float(p.balance) - min(float(paid), float(p.balance))
        p.save()
        return redirect('ras_pi_admin:index')
    else:
        raise Http404()


def restock(request):
    if request.POST:
        param = request.POST
        p = Product.objects.get(pk=param['product_id'])
        p.in_stock = int(param['new_stock'])
        p.save()
        return index(request)
    else:
        raise Http404()


def change_price(request):
    if request.POST:
        param = request.POST
        p = Product.objects.get(pk=param['product_id'])
        p.price = float(param['new_price'])
        p.save()
        return index(request)
    else:
        raise Http404()


def new_product(request):
    return render(request, 'ras_pi_admin/new_product.html')


def new_product_post(request):
    r = request.POST
    if r:
        print(r)
        p = Product(name=r['name'], price=r['price'], in_stock=r['stock'])
        p.save()
    else:
        raise Http404()

    return index(request)


def product_list_search(request):
    p = Product.objects.all()
    return render(request, 'ras_pi_admin/product_list_search.html', {'products':p})


def product_list_search_post(request):
    if request.POST:
        search_term = request.POST['search_term']
        p = Product.objects.filter(name__icontains=search_term)
        return render(request, 'ras_pi_admin/product_list_search.html', {'products':p, 'search_term':search_term})
    else:
        raise Http404()


def profile_list_search(request):
    p = Profile.objects.all()
    return render(request, 'ras_pi_admin/profile_list_search.html', {'profiles':p})


def profile_list_search_post(request):
    if request.POST:
        search_term = request.POST['search_term']
        p = Profile.objects.filter(user__username__icontains=search_term)
        return render(request, 'ras_pi_admin/profile_list_search.html', {'profiles':p, 'search_term':search_term})
    else:
        raise Http404()


def sale_list(request):
    s = Sale.objects.all()
    return render(request, 'ras_pi_admin/sale_list.html', {'sales':s})


def get_need_to_pay():
    profile_q = Profile.objects.filter(balance__gte=10)
    profile_lst = []
    for profile in profile_q:
        profile_lst.append(profile)

    return profile_lst


def get_need_to_restock():
    product_q = Product.objects.filter(in_stock__lte=5)
    product_lst = []
    for product in product_q:
        product_lst.append(product)

    return product_lst
