from django.conf.urls import url

from . import views


app_name = 'ras_pi_admin'
urlpatterns = [
    # ex: /ras_pi_admin/
    url(r'^$', views.index, name='index'),
    # ex: /ras_pi_admin/profile_summary/8
    url(r'^profile_summary/(?P<profile_id>[0-9]+)$', views.profile_summary, name='profile_summary'),
    url(r'^product_summary/(?P<product_id>[0-9]+)$', views.product_summary, name='product_summary'),
    url(r'^pay_balance$', views.pay_balance, name='pay_balance'),
    url(r'^restock$', views.restock, name='restock'),
    url(r'^change_price$', views.change_price, name='change_price'),
    url(r'^new_product$', views.new_product, name='new_product'),
    url(r'^new_product_post$', views.new_product_post, name='new_product_post'),
    url(r'^product_list_search$', views.product_list_search, name='product_list_search'),
    url(r'^product_list_search_post$', views.product_list_search_post, name='product_list_search_post'),
    url(r'^profile_list_search$', views.profile_list_search, name='profile_list_search'),
    url(r'^profile_list_search_post$', views.profile_list_search_post, name='profile_list_search_post'),
    url(r'^sale_list$', views.sale_list, name='sale_list'),
]
