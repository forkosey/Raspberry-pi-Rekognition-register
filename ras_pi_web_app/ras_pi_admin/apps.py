from django.apps import AppConfig


class RasPiAdminConfig(AppConfig):
    name = 'ras_pi_admin'
