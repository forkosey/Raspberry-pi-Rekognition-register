from django.conf.urls import url

from . import views


app_name = 'ras_pi_ui'
urlpatterns = [
    # ex: /ras_pi_ui/
    url(r'^$', views.index, name='index'),
    # ex: /ras_pi_ui/picture_taking/
    url(r'^picture_taking/$', views.picture_taking, name="picture_taking"),
    # ex: /ras_pi_ui/auth_positive/
    url(r'^auth_positive/$', views.auth_positive, name="auth_positive"),
    # ex: /ras_pi_ui/profile_summary/
    url(r'^profile_summary/$', views.profile_summary, name="profile_summary"),
    # ex: /ras_pi_ui/register/
    url(r'^register/$', views.register, name="register"),
    url(r'^register_post/$', views.register_post, name="register_post"),
    # ex: /ras_pi_ui/login/
    url(r'^app_login/$', views.app_login, name="app_login"),
    url(r'^app_login_post/$', views.app_login_post, name="app_login_post"),
    # ex: /ras_pi_ui/get_copy_path/
    url(r'^get_copy_path/(?P<key>[0-9A-Za-z]+)$', views.get_copy_path, name="get_copy_path"),
]
