from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    balance = models.DecimalField('Balance to be paid', max_digits=6, decimal_places=2, default=0)
    reko_face_id = models.CharField(max_length=36, default='')
    reko_desc = models.CharField(max_length=150, default='')

    def __str__(self):
        return self.user.username


class Product(models.Model):
    name = models.CharField(max_length=25)
    price = models.DecimalField(max_digits=5, decimal_places=2, default=0)
    in_stock = models.SmallIntegerField('Quantity in stock', default=0)

    def __str__(self):
        return self.name


class Sale(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    date = models.DateTimeField('Date purchased', default=timezone.now)

    def __str__(self):
        return str(self.user) + ' --> ' + str(self.product) +\
        ' ( ' + str(self.product.price) + '$ )'


class AwsDescriptionTopThree(models.Model):
    profile = models.ForeignKey(Profile, on_delete=models.CASCADE)
    date = models.TimeField('Date of creation', default=timezone.now)


class AwsDescription(models.Model):
    top_three = models.ForeignKey(AwsDescriptionTopThree, on_delete=models.CASCADE)
    attribute_name = models.CharField(max_length=10)
    attribute_value = models.CharField(max_length=10)

    def __str__(self):
        return str(self.attribute_name) + '-->' + str(self.attribute_value)
