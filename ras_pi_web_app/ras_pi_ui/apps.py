from django.apps import AppConfig


class RasPiUiConfig(AppConfig):
    name = 'ras_pi_ui'
