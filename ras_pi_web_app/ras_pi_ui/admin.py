from django.contrib import admin

from .models import Profile, Product, Sale, AwsDescription, AwsDescriptionTopThree

admin.site.register(Profile)
admin.site.register(Product)
admin.site.register(Sale)
admin.site.register(AwsDescription)
admin.site.register(AwsDescriptionTopThree)
