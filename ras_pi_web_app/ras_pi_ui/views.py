# -*- coding: utf-8 -*-

import io
import random
import string
from operator import itemgetter
from os import path
from time import sleep
from shutil import copyfile
from picamera import PiCamera, Color
from PIL import Image
import requests

from Rekognition.reko_collection import (add_face_to_collection,
                                         search_faces_from_img, download_profile_img)

from Rekognition.exceptions import NoMatchingFaceException, NoFaceDetectedException
from django.shortcuts import render, redirect, Http404
from django.http import JsonResponse
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.core.cache import cache
from ras_pi.settings import MEDIA_ROOT
from .models import Profile, Product, Sale, AwsDescription, AwsDescriptionTopThree


camera = PiCamera()
BASE = path.dirname(path.abspath(__file__))
FACE_PICTURE_PATH = MEDIA_ROOT + "/media/new_picture/face_picture.jpg"
FACE_PICTURE_PATH_STATIC = MEDIA_ROOT + "/ras_pi_ui/static/ras_pi_ui/face_picture.jpg"
PROFILE_PICTURE_STATIC = MEDIA_ROOT + "/ras_pi_ui/static/ras_pi_ui/profile_picture.jpg"
RANDOM_KEY = ""

def index(request):
    try:
        if request.user.is_authenticated:
            logout(request)
    except AttributeError:
        pass

    return render(request, 'ras_pi_ui/index.html', {'weather_info': get_weather()})


def get_weather():
    req = requests.get('http://api.openweathermap.org/data/2.5/weather?lat=46.81&lon=-71.22&' +\
                       'units=metric&lang=fr&appid=8974382f7039d2d271a378d92900179f').json()

    weather_info = {'img_id': req['weather'][0]['icon'], 'name': req['name'],
                    'country': req['sys']['country'],
                    'min': str(req['main']['temp_min']), 'max': str(req['main']['temp_max']),
                    'temp': str(req['main']['temp']), 'desc': req['weather'][0]['description']}

    return weather_info


def get_copy_path(request, key):
    if key == RANDOM_KEY:
        return JsonResponse({'path':PROFILE_PICTURE_STATIC})

    return JsonResponse({'error':'Wrong key or no key provided.'})


def picture_taking(request):
    take_picture()
    response = face_recognition_auth(request)
    if response is True:
        return auth_positive(request)
    elif response == 2:
        return register(request)
    elif response == 3:
        return render(request, 'ras_pi_ui/index.html', {'weather_info': get_weather(),
                                                        'no_face':True})


def face_recognition_auth(request):
    byte_img_io = io.BytesIO()

    #ras_pi loading path
    byte_img = Image.open(FACE_PICTURE_PATH)
    #loading path for pc testing
    #byte_img = open_image_for_pc_test()

    byte_img.save(byte_img_io, "PNG")
    byte_img_io.seek(0)
    byte_img = byte_img_io.read()
    return collection_method(byte_img, request)



def auth_positive(request):
    items = Product.objects.all()
    profile = Profile.objects.get(user=request.user)
    return render(request, 'ras_pi_ui/auth_positive.html', {'items': items,
                                                            'f_name':request.user.first_name,
                                                            'l_name':request.user.last_name,
                                                            'desc':profile.reko_desc.split('\n'),
                                                            'login':True})


def profile_summary(request):
    if request.POST:

        product_id = request.POST['product_choice']
        prod = Product.objects.filter(id=product_id)[0]
        profile = Profile.objects.filter(user=request.user)[0]

        new_sale = Sale(product=prod, user=request.user)
        new_sale.save()

        profile.balance += prod.price
        prod.in_stock -= 1
        profile.save()
        prod.save()

        logout(request)

        return render(request, 'ras_pi_ui/profile_summary.html', {'profile':profile,
                                                                  'product':prod})


def app_login(request):
    return render(request, 'ras_pi_ui/login.html')


def app_login_post(request):
    if request.POST:
        param = request.POST
        user = authenticate(request,
                            username=param['inputUsername'],
                            password=param['inputPassword'])

        if user is not None:
            login(request, user)
            return redirect('ras_pi_ui:auth_positive')

        return render(request, 'ras_pi_ui/login.html', {'error':True})


def register(request):
    return render(request, 'ras_pi_ui/register.html')


def register_post(request):
    if request.POST:
        param = request.POST
        email = param['inputEmail']
        first_name = email[:email.index('.')]
        last_name = email[email.index('.')+1:email.index('@')]
        first_name = put_capitals(first_name)
        last_name = put_capitals(last_name)
        user = User.objects.create_user(username=email[:email.index('@')],
                                        email=email, password='qwer1234',
                                        first_name=first_name,
                                        last_name=last_name)
        profile = Profile(user=user)
        profile.save()
        byte_img_io = io.BytesIO()
        # open the raspi image
        byte_img = Image.open(FACE_PICTURE_PATH_STATIC)
        #Pc testing
        #byte_img = open_image_for_pc_test()

        byte_img.save(byte_img_io, "PNG")
        byte_img_io.seek(0)
        byte_img = byte_img_io.read()
        reko_response = add_face_to_collection(byte_img, 'profile_img_'+str(profile.id)+'.png')
        profile.reko_face_id = reko_response['face_id']
        profile.reko_desc = aws_desc_dict_to_string(reko_response['aws_desc_dict'])
        profile.save()
        create_api_data(aws_desc_dict=reko_response['aws_desc_dict'], profile=profile)
        login(request, profile.user)

        return render(request, 'ras_pi_ui/auth_positive.html',
                      {'items' : Product.objects.all(),
                       'f_name': profile.user.first_name,
                       'l_name': profile.user.last_name,
                       'desc'  : profile.reko_desc.split('\n'),
                       'login' : False}
                     )

    else:
        raise Http404()



def take_picture():
    camera.rotation = 270
    camera.resolution = (800, 480)
    camera.annotate_text = "La photo sera prise dans 3 secondes!"
    camera.annotate_foreground = Color('white')
    camera.annotate_text_size = 80
    camera.start_preview()
    sleep(3)
    camera.annotate_text = ""
    camera.capture(FACE_PICTURE_PATH)
    camera.stop_preview()
    copyfile(FACE_PICTURE_PATH, FACE_PICTURE_PATH_STATIC)
    #PC testing
    #return None


def collection_method(img, request):
    try:
        reko_response = search_faces_from_img(img)
        profile = Profile.objects.filter(reko_face_id=reko_response['face_id'])[0]
        profile.reko_desc = aws_desc_dict_to_string(reko_response['aws_desc_dict'])
        profile.save()

        create_api_data(aws_desc_dict=reko_response['aws_desc_dict'], profile=profile)

        #download_profile_img('profile_img_'+str(profile.id)+'.png', PROFILE_PICTURE_STATIC)
        login(request, profile.user)
        return True
    except NoMatchingFaceException:
        return 2
    except NoFaceDetectedException:
        return 3


def key_gen(lenght=32):
    global RANDOM_KEY
    RANDOM_KEY = ''.join(random.choices(string.ascii_letters + string.digits, k=lenght))


def put_capitals(name):
    name = name[0].upper() + name[1:]
    if '-' in name:
        name = name[:name.index('-')+1] + name[name.index('-')+1].upper() + name[name.index('-')+2:]

    return name


def aws_desc_dict_to_string(aws_dict_desc):
    string_desc = 'Age: De ' + str(aws_dict_desc['AgeRange']['Low']) +\
                  ' a ' + str(aws_dict_desc['AgeRange']['High']) + ' ans' +\
                  '\nBarbe: ' + simple_data_to_text(aws_dict_desc['Beard']) +\
                  '\nEmotion: ' + aws_dict_desc['Emotions'][0]['Type'] +\
                  ' (' + str(round(aws_dict_desc['Emotions'][0]['Confidence'], 2)) + '%)' +\
                  '\nLunettes: ' + simple_data_to_text(aws_dict_desc['Eyeglasses']) +\
                  '\nYeux ouverts: ' + simple_data_to_text(aws_dict_desc['EyesOpen']) +\
                  '\nSexe: ' + simple_data_to_text(aws_dict_desc['Gender']) +\
                  '\nBouche ouverte: ' + simple_data_to_text(aws_dict_desc['MouthOpen']) +\
                  '\nMoustache: ' + simple_data_to_text(aws_dict_desc['Mustache']) +\
                  '\nSourire: ' + simple_data_to_text(aws_dict_desc['Smile']) +\
                  '\nLunettes Fumees: ' + simple_data_to_text(aws_dict_desc['Sunglasses'])

    return string_desc


def simple_data_to_text(data):
    return change_bool_to_text(data['Value']) + ' (' + str(round(data['Confidence'], 2)) + '%)'


def change_bool_to_text(bool_value):
    if bool_value is True:
        return 'Oui'
    elif bool_value is False:
        return 'Non'

    return bool_value

def trad_aws_emotion(emotion, gender):
    if emotion == 'HAPPY':
        if gender == 'Female':
            return 'Heureuse'
        return 'Heureux'
    elif emotion == 'SAD':
        return 'Triste'
    elif emotion == 'ANGRY':
        if gender == 'Female':
            return 'Fachee'
        return 'Fache'
    elif emotion == 'CONFUSED':
        if gender == 'Female':
            return 'Confuse'
        return 'Confus'
    elif emotion == 'DISGUSTED':
        if gender == 'Female':
            return 'Degoutee'
        return 'Degoute'
    elif emotion == 'SURPRISED':
        if gender == 'Female':
            return 'Surprise'
        return 'Surpris'

def prepare_api_data_from_aws_desc_dict(aws_desc_dict):
    del aws_desc_dict['AgeRange']
    del aws_desc_dict['Eyeglasses']
    del aws_desc_dict['MouthOpen']
    del aws_desc_dict['EyesOpen']
    aws_desc_dict['Emotions'] = max(aws_desc_dict['Emotions'], key=lambda x: x['Confidence'])
    result_dict = {}
    for key, value in aws_desc_dict.items():
        result_dict[key] = value['Confidence']

    most_confident_attributes = sorted(result_dict.items(), key=itemgetter(1))[-3:]
    return_list = []
    for attribute in most_confident_attributes:
        if attribute[0] == 'Emotions':
            return_list.append({'attribute_name':attribute[0],
                                'atttribute_value':trad_aws_emotion(
                                    aws_desc_dict[attribute[0]]['Type'],
                                    aws_desc_dict['Gender'])})
        else:
            return_list.append({'attribute_name':attribute[0],
                                'attribute_value':change_bool_to_text(
                                    aws_desc_dict[attribute[0]]['Value'])})

    return return_list


def create_api_data(aws_desc_dict, profile):
    api_desc_data = prepare_api_data_from_aws_desc_dict(aws_desc_dict)
    AwsDescriptionTopThree.objects.filter(profile=profile).delete()
    top_three = AwsDescriptionTopThree(profile=profile)
    top_three.save()
    for attribute in api_desc_data:
        AwsDescription.objects.create(top_three=top_three,
                                      attribute_name=attribute['attribute_name'],
                                      attribute_value=attribute['attribute_value'])


def open_image_for_pc_test():
    test_img_path = BASE + '/test_img/'
    #byte_img = Image.open(test_img_path + 'tom_searle_3.jpg')
    #byte_img = Image.open(test_img_path + 'john_mayer_2.jpg')
    byte_img = Image.open(test_img_path + 'will_ferrell_3.jpg')
    return byte_img
